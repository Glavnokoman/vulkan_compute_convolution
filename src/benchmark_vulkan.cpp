#include "cross_correlation_cpu.hpp"
#include "cross_correlation.hpp"

#include <benchmark/benchmark.h>

std::vector<float>
getKernel(size_t size)
{
  std::vector<float> kernel(size*size);
  std::iota(kernel.begin(), kernel.end(), 0);

  return kernel;
}

std::vector<float>
getInput(size_t size)
{
  std::vector<float> input(size*size);
  std::iota(input.begin(), input.end(), 0);

  return input;
}

static void
CPU_Convolution(benchmark::State& state)
{
  const auto inputSize { state.range(0) };
  const auto kernelSize { state.range(1) };

  const auto input = getInput(inputSize);
  const auto kernel = getKernel(kernelSize);

  const size_t outputSize = inputSize - (kernelSize-1);
  std::vector<float> output(outputSize*outputSize);

  for (auto _ : state) {
    benchmark::DoNotOptimize(
    validCrossCorrelation2DCPU(input.data(), inputSize, kernel.data(),
      kernelSize, output)
    );
  }
}
BENCHMARK(CPU_Convolution)->Ranges({{128, 1024}, {3, 5}});

static void
GPU_Convolution(benchmark::State& state)
{
  const auto inputSize { state.range(0) };
  const auto kernelSize { state.range(1) };

  const int workgroupSize { 64 };

  const auto input = getInput(inputSize);
  const auto kernel = getKernel(kernelSize);

  ValidCrossCorrelation2DVulkan xcorr(inputSize, kernelSize, workgroupSize);

  xcorr.setInput(input);
  xcorr.setKernel(kernel);

  for (auto _ : state) {
    xcorr.run();
  }
}
BENCHMARK(GPU_Convolution)->Ranges({{128, 1024}, {3, 5}});

BENCHMARK_MAIN();
