#include "cross_correlation_cpu.hpp"

#include <vuh/array.hpp>
#include <vuh/vuh.h>

#include <numeric>
#include <chrono>
#include <getopt.h>
#include <iostream>
#include <iomanip>
#include <vector>

template <typename T>
static void
printArray(const T array, size_t size)
{
  assert(array.size() == size * size);
  for (size_t y=0; y<size; ++y) {
    std::cout << "| ";
    for (size_t x=0; x<size; ++x) {
      std::cout << std::setw(10) << std::setprecision(3)
        << array[x + size*y] << ' ';
    }
    std::cout << "|\n";
  }
}

// http://www.floating-point-gui.de/errors/comparison/
template <typename T>
static constexpr bool nearlyEqual(T a, T b, T epsilon) {
  const float diff = std::abs(a - b);

  if (a == b) // shortcut, handles infinities
    return true;

  if (a == 0 || b == 0 || diff < std::numeric_limits<T>::min()) {
    // a or b is zero or both are extremely close to it
    // relative error is less meaningful here
    return diff < (epsilon * std::numeric_limits<T>::min());
  } else { // use relative error
    return diff / std::min(std::abs(a) + std::abs(b),
        std::numeric_limits<T>::max()) < epsilon;
  }
}

  int
main(int argc, char** argv)
{
  uint32_t kernelSize     { 3 };
  uint32_t inputSize     { 16 };
  uint32_t workgroupSize { 16 };

  bool verbose = false;

  int opt;
  while ((opt = getopt(argc, argv, "k:i:w:v")) != -1) {
    switch (opt) {
      case 'k':
        kernelSize = std::stoi(optarg);
        break;
      case 'i':
        inputSize = std::stoi(optarg);
        break;
      case 'w':
        workgroupSize = std::stoi(optarg);
        break;
      case 'v':
        verbose = true;
        break;

      default:
        return EXIT_FAILURE;
    }
  }

  if (optind != argc) {
    return EXIT_FAILURE;
  }


//  ValidCrossCorrelation2DVulkan xcor(inputSize, kernelSize, workgroupSize);

  try {
    std::vector<float> kernel(kernelSize*kernelSize);
    std::vector<float> compute_input(inputSize*inputSize);

    std::iota(compute_input.begin(), compute_input.end(), 0);
    std::iota(kernel.begin(), kernel.end(), 0);

	 const auto output_size = inputSize - (kernelSize - 1);
	 auto instance = vuh::Instance();
	 auto device = instance.devices().at(0);

	 auto d_kernel = vuh::Array<float>(device, kernel);
	 auto d_input = vuh::Array<float>(device, compute_input);
	 auto d_output = vuh::Array<float>(device, output_size*output_size);

	 using Specs = vuh::typelist<uint32_t, uint32_t, uint32_t>;
	 auto program = vuh::Program<Specs>(device, "shaders/cross_correlation.comp.spv");

	 const auto grid_size = vuh::div_up(inputSize, workgroupSize);
	 program.grid(grid_size, grid_size)
	       .spec(kernelSize, output_size, workgroupSize)
	       .bind(d_input, d_kernel, d_output);

//    std::cout << "actual workgroup size: " << xcor.workGroupSize() << '\n';

    auto startGPU = std::chrono::high_resolution_clock::now();
    program.run();
    auto endGPU = std::chrono::high_resolution_clock::now();

	 auto output = d_output.toHost<std::vector<float>>();
//    auto output = xcor.getOuptput();

    using miliseconds = std::chrono::duration<float, std::chrono::milliseconds::period>;
    std::cout << "elapsed time GPU: " << miliseconds(endGPU-startGPU).count() << " ms\n";


    const size_t outputSize = inputSize - (kernelSize-1);
    std::vector<float> expectedOutput(outputSize*outputSize);
    auto start = std::chrono::high_resolution_clock::now();
    validCrossCorrelation2DCPU(compute_input.data(), inputSize, kernel.data(),
        kernelSize, expectedOutput);
    auto end = std::chrono::high_resolution_clock::now();

    std::cout << "elapsed time CPU: " << miliseconds(end-start).count() << " ms\n";

    if (verbose) {
      std::cout << "input: \n";
      printArray(compute_input, inputSize);
      std::cout << "\n\n";

      std::cout << "kernel: \n";
      printArray(kernel, kernelSize);
      std::cout << "\n\n";

      std::cout << "output: \n";
//      printArray(output, xcor.outputSize());
    }

//    assert(expectedOutput.size() == xcor.outputSize()*xcor.outputSize());
    if (std::equal(expectedOutput.begin(), expectedOutput.end(),
          output.begin(),
          [](auto a, auto b) { return nearlyEqual(a, b, 1e-5f); })) {
      std::cout << "\noutput check: success\n";
    } else {
      std::cerr << "\noutput check: failed!\n";
      if (verbose) {
        std::cout << "expected output: \n";
//        printArray(expectedOutput, xcor.outputSize());
        std::vector<float> diff(output.size());
        std::transform(output.begin(), output.end(),
            expectedOutput.begin(), diff.begin(),
            [](auto a, auto b) { return a-b; });
        std::cout << "diff: \n";
//        printArray(diff, xcor.outputSize());
      }
      return EXIT_FAILURE;
    }

  } catch (const std::runtime_error& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
