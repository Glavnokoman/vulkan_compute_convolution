#pragma once

#include <cassert>
#include <cstddef>
#include <vector>

template <typename T>
std::vector<T>
validCrossCorrelation2DCPU(const T* input, size_t inputSize,
    const T* kernel, size_t kernelSize, std::vector<T>& output)
{
  const size_t outputSize = inputSize - (kernelSize-1);
  assert(output.size() == outputSize*outputSize);

  for (size_t y=0; y<outputSize; ++y) {
    for (size_t x=0; x<outputSize; ++x) {
      T sum(0);
      for (size_t ky=0; ky<kernelSize; ++ky) {
        for (size_t kx=0; kx<kernelSize; ++kx) {
          sum += kernel[ky   *kernelSize + kx]
               * input[(y+ky)*inputSize  + x + kx];
        }
      }
      output[y*outputSize + x] = sum;
    }
  }

  return output;
}
