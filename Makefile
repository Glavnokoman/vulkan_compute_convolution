GLS_COMPILER := glslangValidator

LDFLAGS := -lvulkan
CXXFLAGS := -std=c++14 -Wall

shaders := shaders/cross_correlation.comp.spv

objects := cross_correlation $(shaders)

all: cross_correlation
cross_correlation: CXXFLAGS += -g
cross_correlation: $(shaders)


%: src/%.cpp src/*.hpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $<

shaders/%.spv: shaders/%
	$(GLS_COMPILER) -o $@ -V $<

# test
.PHONY: test
test: cross_correlation
	./$< > /dev/null
	./$< -i 128 -k 5 -w 64 > /dev/null
	./$< -i 1024 -k 7 -w 64 > /dev/null
	@echo "success"

# benchmark
benchmark_vulkan: LDFLAGS += -lbenchmark
benchmark_vulkan: CXXFLAGS += -O3 -DNDEBUG
objects += benchmark_vulkan

benchmark/benchmark_vulkan.csv: benchmark_vulkan
	./$< --benchmark_repetitions=10 --benchmark_format=csv > $@
objects += benchmark/benchmark_vulkan.csv

.PHONY: benchmark/convolution_cl/
benchmark/convolution_cl/:
	$(MAKE) -C $@

benchmark/Rplot001.svg: benchmark/analyze.r benchmark/benchmark_vulkan.csv \
	benchmark/convolution_cl/
	cd benchmark && ./analyze.r

.PHONY: plot-benchmark
plot-benchmark: benchmark/Rplot001.svg
	
.PHONY: clean
clean:
	rm -f $(objects) benchmark/Rplot*
	$(MAKE) -C benchmark/convolution_cl/ clean

.PHONY: tidy
tidy:
	clang-tidy -checks=* src/*.cpp -- ${CXXFLAGS}
